<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\SignController;
use \App\Http\Controllers\BlogController;
use \App\Http\Controllers\CategoryController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'sign'], function () {
    Route::post('/up/step1', [SignController::class, 'up_step1']);
    Route::post('/up/step2', [SignController::class, 'up_step2']);
    Route::post('/up/step3', [SignController::class, 'up_step3']);

    Route::post('/in', [SignController::class, 'in']);

    Route::post('/forgot', [SignController::class, 'forgot']);
    Route::post('/reset', [SignController::class, 'reset'])->name('password.reset');
});



// Blogs API
Route::group(['prefix' => 'blogs'], function () {
    Route::get('/', [BlogController::class, 'index']);
    Route::get('/{id}', [BlogController::class, 'show']);

    Route::middleware('auth:api')->post('/', [BlogController::class, 'store']);
    Route::middleware('auth:api')->delete('/{id}', [BlogController::class, 'delete']);
    Route::middleware('auth:api')->post('/{id}', [BlogController::class, 'update']);
});


// Categories API
Route::group(['prefix' => 'categories'], function () {
    Route::get('/', [CategoryController::class, 'index']);
    Route::get('/{id}', [CategoryController::class, 'show']);

    Route::middleware('auth:api')->post('/', [CategoryController::class, 'store']);
    Route::middleware('auth:api')->delete('/{id}', [CategoryController::class, 'delete']);
    Route::middleware('auth:api')->post('/{id}', [CategoryController::class, 'update']);
});
