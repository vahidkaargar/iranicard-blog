<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id')
            ->select(['id', 'name']);

    }

    public function blogs()
    {
        return $this->hasMany(Blog::class, 'category_id')
            ->select(['id', 'category_id', 'user_id', 'title']);
    }
}
