<?php

namespace App\Http\Controllers;

use App\Http\Packages\ResponseCustom;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    public function index()
    {
        return Category::with(['user', 'blogs.user'])->latest('id')->simplePaginate(10);
    }

    public function store(Request $request)
    {
        $input = $request->all(['name']);
        // Validation rules
        $validator = Validator::make($input, [
            'name' => 'required|max:255',
        ]);

        // Results of Validation
        if($validator->fails())
        {
            return ResponseCustom::sendError('Validation Error.', $validator->errors());
        }

        // Retrieve user information
        $user = Auth::user();
        $input['user_id'] = $user->id;

        // creating blog
        $create = Category::create($input);
        if(is_null($create))
        {
            return ResponseCustom::sendError('Something wrong with connecting to database');
        }
        return ResponseCustom::sendResponse($create, 'Your category has been created.');
    }

    public function update($id)
    {
        $input = request()->all(['name']);
        // Validation rules
        $validator = Validator::make($input, [
            'name' => 'required|max:255',
        ]);

        // Results of Validation
        if($validator->fails())
        {
            return ResponseCustom::sendError('Validation Error.', $validator->errors());
        }


        // Check category
        $Category = Category::where('id', $id);
        if(!$Category->exists())
        {
            return ResponseCustom::sendError('Category not found.');
        }
        $Category = $Category->first();

        // Retrieve user information
        $user = Auth::user();

        // Check if user is owner
        if($Category->user_id != $user->id)
        {
            return ResponseCustom::sendError('You\'re not the owner of this category');
        }

        // Updating category
        $Category->name = $input['name'];
        if(!$Category->save())
        {
            return ResponseCustom::sendError('Something wrong with connecting to database');
        }
        return ResponseCustom::sendResponse(['id' => $Category->id], 'Your category has been updated.');
    }

    public function delete($id)
    {
        $user = Auth::user();
        if(Category::where('id', $id)->where('user_id', $user->id)->delete())
            return ResponseCustom::sendResponse([], 'Your category has been deleted.');
        else
            return ResponseCustom::sendError('This category may not exists or you dont have permission to delete.');
    }
}
