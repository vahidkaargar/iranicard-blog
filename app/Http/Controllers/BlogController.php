<?php

namespace App\Http\Controllers;

use App\Http\Packages\ResponseCustom;
use App\Models\Blog;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class BlogController extends Controller
{
    public function index(Request $request)
    {
        // Safe select of get params
        $input = $request->all(['filter_by', 'filter_value']);

        $Blogs = Blog::with(['user', 'category']);

        // Make filters for results
        if($input['filter_by'] == 'category')
            $Blogs = $Blogs->where('category_id', $input['filter_value']);
        elseif($input['filter_by'] == 'user')
            $Blogs = $Blogs->where('user_id', $input['filter_value']);
        elseif($input['filter_by'] == 'search')
            $Blogs = $Blogs->where('body', 'like', '%'.$input['filter_value'].'%');

        return $Blogs->latest('updated_at')->simplePaginate(10);
    }

    public function store(Request $request)
    {
        $input = $request->all(['category_id', 'title', 'body']);
        // Validation rules
        $validator = Validator::make($input, [
            'category_id' => 'required|integer',
            'title' => 'required|max:255',
            'body' => 'required',
        ]);

        // Results of Validation
        if($validator->fails())
        {
            return ResponseCustom::sendError('Validation Error.', $validator->errors());
        }

        // Check category
        $Category = Category::where('id', $input['category_id']);
        if(!$Category->exists())
        {
            return ResponseCustom::sendError('Category not found.');
        }

        // Retrieve user information
        $user = Auth::user();
        $input['user_id'] = $user->id;

        // creating blog
        $create = Blog::create($input);
        if(is_null($create))
        {
            return ResponseCustom::sendError('Something wrong with connecting to database');
        }
        return ResponseCustom::sendResponse($create, 'Your blog has been created.');
    }

    public function update($id)
    {
        $input = request()->all(['category_id', 'title', 'body']);
        // Validation rules
        $validator = Validator::make($input, [
            'category_id' => 'required|integer',
            'title' => 'required|max:255',
            'body' => 'required',
        ]);

        // Results of Validation
        if($validator->fails())
        {
            return ResponseCustom::sendError('Validation Error.', $validator->errors());
        }


        // Check category
        $Category = Category::where('id', $input['category_id']);
        if(!$Category->exists())
        {
            return ResponseCustom::sendError('Category not found.');
        }


        // Check blog if exists
        $Blog = Blog::where('id', $id);
        if(!$Blog->exists())
        {
            return ResponseCustom::sendError('This blog not exists.');
        }
        $Blog = $Blog->first();


        // Retrieve user information
        $user = Auth::user();

        // Check if user is owner
        if($Blog->user_id != $user->id)
        {
            return ResponseCustom::sendError('You\'re not the owner of this blog');
        }

        // Updating blog
        $Blog->title = $input['title'];
        $Blog->body = $input['body'];
        $Blog->category_id = $input['category_id'];

        if(!$Blog->save())
        {
            return ResponseCustom::sendError('Something wrong with connecting to database');
        }
        return ResponseCustom::sendResponse(['id' => $Blog->id], 'Your blog has been updated.');
    }

    public function delete($id)
    {
        $user = Auth::user();
        if(Blog::where('id', $id)->where('user_id', $user->id)->delete())
            return ResponseCustom::sendResponse([], 'Your blog has been deleted.');
        else
            return ResponseCustom::sendError('This blog may not exists or you dont have permission to delete.');
    }
}
