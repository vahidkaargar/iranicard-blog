<?php

namespace App\Http\Controllers;
use App\Http\Packages\ResponseCustom;
use App\Models\User;
use App\Models\UserToken;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;
use Mail;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;



class SignController extends Controller
{
    public function up_step1(Request $request)
    {
        // Validation rules
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        // Results of Validation
        if($validator->fails())
        {
            return ResponseCustom::sendError('Validation Error.', $validator->errors());
        }

        // Create user
        $User = User::firstOrCreate([
            'email' => $request->get('email')
        ]);

        // check if user's email created and verified
        if($User->registration_status == 'STEP3')
        {
            return ResponseCustom::sendError('This email registered already.');
        }
        else
        {
            $random_code = mt_rand(10000, 99999);
            $User->email_verification_code = $random_code;
            $User->email_verification_code_expired_at = Carbon::now()->addDay(); // Add one day to expire this code
            $User->registration_status = 'STEP1';
            $User->save();

            // Send verification code to user's email
            Mail::send('emails.sign_up_step1', ['email' => $User->email, 'code' => $random_code], function ($message) use ($User) {
                $message->from(env('EMAIL_ADDRESS'), env('APP_NAME'));
                $message->to($User->email)->subject('Registration code');
            });

            return ResponseCustom::sendResponse(['email' => $User->email], 'User register step 1 completed.');
        }
    }

    public function up_step2(Request $request)
    {
        // Validation rules
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'verification_code' => 'required|digits:5',
        ]);

        // Results of Validation
        if($validator->fails())
        {
            return ResponseCustom::sendError('Validation Error.', $validator->errors());
        }


        // Check if email exists and code not expired
        $User = User::where('email', $request->post('email'))
            ->where('registration_status', '<>', 'STEP3')
            ->whereDate('email_verification_code_expired_at', '>=', Carbon::now());
        if(!$User->exists())
        {
            return ResponseCustom::sendError('Your code has been expired, go to first step.');
        }
        $User = $User->first();

        // Compare verification code
        if(strcmp($User->email_verification_code, $request->post('verification_code')) !== 0)
        {
            return ResponseCustom::sendError('Your verification code is wrong');
        }
        else
        {
            // Save verified date for user
            $User->email_verified_at = Carbon::now();
            $User->registration_status = 'STEP2';
            $User->save();

            // Step 3 need a token for update just by the related user
            // To make token unrelated to the database, We use a combination of the following keys
            $step3_token = md5($User->id . $User->email . $User->email_verification_code);

            $success = [
                'email' => $User->email,
                'step3_token' => $step3_token
            ];
            return ResponseCustom::sendResponse($success, 'User register step 2 completed.');
        }
    }

    public function up_step3(Request $request)
    {
        // Validation rules
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'step3_token' => 'required',
            'name' => 'required|string',
            'password' => 'required|min:6|max:32',
        ]);

        // Results of Validation
        if($validator->fails())
        {
            return ResponseCustom::sendError('Validation Error.', $validator->errors());
        }

        // Get user if exists
        $User = User::where('email', $request->post('email'))->where('registration_status', '<>', 'STEP3');
        if(!$User->exists())
        {
            return ResponseCustom::sendError('Can\'t find this user.');
        }
        $User = $User->first();

        // Check step 3 token that we get from step 2
        $check_step3_token = md5($User->id . $User->email . $User->email_verification_code);
        if(strcmp($check_step3_token, $request->post('step3_token')) !== 0)
        {
            return ResponseCustom::sendError('You\'re not authorised for this step.', ['error'=> 'Unauthorised'], 401);
        }

        // Update user information
        $User->name = $request->get('name');
        $User->password = bcrypt($request->get('password'));
        $User->email_verification_code = null;
        $User->registration_status = 'STEP3';
        $User->save();


        // Send verification code to user's email
        Mail::send('emails.sign_up_step3', ['email' => $User->email, 'password' => $request->get('password')], function ($message) use ($User) {
            $message->from(env('EMAIL_ADDRESS'), env('APP_NAME'));
            $message->to($User->email)->subject('Registration completed');
        });

        $success = [
            'email' => $User->email,
            'token' => $User->createToken(env('TOKEN_NAME'))->accessToken // with this token you can dog to dashboard immediately after registration
        ];
        return ResponseCustom::sendResponse($success, 'Your registration has been completed');
    }


    public function in(Request $request)
    {
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password]))
        {
            // Retrieve user information
            $user = Auth::user();

            // Check if user completed all 3 steps
            if($user->registration_status != 'STEP3')
            {
                return ResponseCustom::sendError('Unauthorised.', ['error'=>'Unauthorised'], 401);
            }

            $success['token'] =  $user->createToken(env('TOKEN_NAME'))->accessToken;
            return ResponseCustom::sendResponse($success, 'User login successfully.');
        }
        else
        {
            return ResponseCustom::sendError('Unauthorised.', ['error'=>'Unauthorised'], 401);
        }
    }

    public function forgot(Request $request)
    {
        $input = $request->all(['email']);
        // Validation rules
        $validator = Validator::make($input, [
            'email' => 'required|email',
        ]);

        // Results of Validation
        if($validator->fails())
        {
            return ResponseCustom::sendError('Validation Error.', $validator->errors());
        }

        // Sending reset link to user email
        if(Password::sendResetLink($input) == Password::PASSWORD_RESET)
            return ResponseCustom::sendResponse([], 'Reset password link has been sent to your email.');
        else
            return ResponseCustom::sendError('Something wrong with sending reset link.');
    }

    public function reset(Request $request)
    {
        $input = $request->all(['email','token', 'password', 'password_confirmation']);

        // Validation rules
        $validator = Validator::make($input, [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6|max:32',
        ]);

        // Results of Validation
        if ($validator->fails())
        {
            return ResponseCustom::sendError('Validation Error.', $validator->errors());
        }

        $reset_password = Password::reset($input, function ($user, $password) {
            $user->password = bcrypt($password);
            $user->save();
        });

        if($reset_password == Password::PASSWORD_RESET)
            return ResponseCustom::sendResponse([], 'Your password has been changed.');
        else
            return ResponseCustom::sendError('Your reset link has been expired.');

    }
}
