<?php

namespace App\Http\Packages;

use Illuminate\Http\Request;

class ResponseCustom
{
    public static function sendResponse($result, $message)
    {
        $response = [
            'success' => true,
            'message' => $message,
            'data'    => $result,
        ];
        return response()->json($response, 200);
    }


    public static function sendError($error, $errorMessages = [], $code = 404)
    {
        $response = [
            'success' => false,
            'message' => $error,
        ];
        if(!empty($errorMessages))
        {
            $response['data'] = $errorMessages;
        }
        return response()->json($response, $code);
    }
}
