## Irani Card Blog
My name is Vahid Kaargar and this project has been developed for interviews with Irani Card.

## Requirement
* Laravel 8.12
* PHP 7.3 >=

## Install
Via Composer
``` bash
$ composer install
```
- Then import `blog_database.sql` to mysql server 
- Put database info in `.env` file

## Postman
You can find [Postman Documenter](https://documenter.getpostman.com/view/9300457/TVep8TLR) here...
